ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

ifdef REG
else
REG := quay.io
endif
ifdef REG_NS
else
REG_NS := hoocloud
endif

ifdef APIKEY__QUAY_IO
#else
#APIKEY__QUAY_IO := quay.io/hoocloud
endif

MAKE := make --no-print-directory

VERSION_FILE := VERSION
VERSION := $(shell cat $(VERSION_FILE) || echo -n '0.0.0' |tee $(VERSION_FILE) )
VERSION_PARTS := $(subst ., ,$(VERSION))

MAJOR := $(word 1,$(VERSION_PARTS))
MINOR := $(word 2,$(VERSION_PARTS))
MICRO := $(word 3,$(VERSION_PARTS))

NEXT_MAJOR := $(shell echo $$(($(MAJOR)+1)))
NEXT_MINOR := $(shell echo $$(($(MINOR)+1)))
NEXT_MICRO := $(shell echo $$(($(MICRO)+1)))

NEXT_VERSION_MICRO := $(MAJOR).$(MINOR).$(NEXT_MICRO)
NEXT_VERSION_MINOR := $(MAJOR).$(NEXT_MINOR).0
NEXT_VERSION_MAJOR := $(NEXT_MAJOR).0.0

container: ver
	#buildah bud --security-opt seccomp=unconfined --layers -t $(REG)/$(REG_NS)/reposync:$(NEXT_VERSION_MICRO)
	buildah bud --layers -t $(REG)/$(REG_NS)/reposync:$(NEXT_VERSION_MICRO)
	buildah tag $(REG)/$(REG_NS)/reposync:$(NEXT_VERSION_MICRO) $(REG)/$(REG_NS)/reposync:latest

.PHONY: ver
ver:
	@$(MAKE) ver-micro

.PHONY: ver-micro
ver-micro:
	@echo -n "$(NEXT_VERSION_MICRO)" > $(VERSION_FILE)
	@echo "$(NEXT_VERSION_MICRO)"

.PHONY: ver-minor
ver-minor:
	@echo -n "$(NEXT_VERSION_MINOR)" > $(VERSION_FILE)
	@echo "$(NEXT_VERSION_MINOR)"

.PHONY: ver-major
ver-major:
	@echo -n "$(NEXT_VERSION_MAJOR)" > $(VERSION_FILE)
	@echo "$(NEXT_VERSION_MAJOR)"


.PHONY: gitpush
gitpush:
	git push 

.PHONY: regpush
regpush:
	curl -X POST https://$(REG)/api/v1/repository -d '{"namespace":"'$(REG_NS)'","repository":"'reposync'","description":"Container image 'reposync'","visibility":"public"}' -H 'Authorization: Bearer '$(APIKEY__QUAY_IO)'' -H "Content-Type: application/json"
	buildah push $(REG)/$(REG_NS)/reposync:$(NEXT_VERSION_MICRO)

.PHONY: regpushrm
regpushrm:
	#podman run --security-opt seccomp=unconfined --rm -t -v $(ROOT_DIR):/myvol -e APIKEY__QUAY_IO='$(APIKEY__QUAY_IO)' chko/docker-pushrm:1 --file /myvol/README.md --provider quay --debug $(REG)/$(REG_NS)/reposync
	podman run --security-opt seccomp=unconfined --rm -t -v $(ROOT_DIR):/myvol -e DOCKER_APIKEY='$(APIKEY__QUAY_IO)' chko/docker-pushrm:1 --file /myvol/README.md --provider quay --debug $(REG)/$(REG_NS)/reposync
