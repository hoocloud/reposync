FROM python:3-slim-buster AS reposync-base

MAINTAINER Holger Fischer <holger.fischer@hoonet.org>

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get -q -y update \
 && apt-get -q -y -o "DPkg::Options::=--force-confold" -o "DPkg::Options::=--force-confdef" dist-upgrade \
 && apt-get -q -y autoremove \
 && apt-get -q -y clean \
 && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*

RUN apt-get -q -y update \
 && apt-get -q -y -o "DPkg::Options::=--force-confold" -o "DPkg::Options::=--force-confdef" install libyaml-dev gcc apt-utils debmirror \
 && apt-get -q -y autoremove \
 && apt-get -q -y clean \
 && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*

FROM reposync-base as reposync-python

COPY requirements.txt /root/
WORKDIR /root
RUN pip3 install -r requirements.txt

FROM reposync-python

RUN mkdir -p /bin /etc/makeDebRepoFromIso/
COPY makeDebRepoFromIso.yml /etc/makeDebRepoFromIso/
RUN chmod 0644 /etc/makeDebRepoFromIso/makeDebRepoFromIso.yml && chown root:root /etc/makeDebRepoFromIso/makeDebRepoFromIso.yml
COPY makeDebRepoFromIso /bin/
RUN chmod 0755 /bin/makeDebRepoFromIso && chown root:root /bin/makeDebRepoFromIso

RUN mkdir -p /bin /etc/cloneDebRepo/
COPY cloneDebRepo.yml /etc/cloneDebRepo/
RUN chmod 0644 /etc/cloneDebRepo/cloneDebRepo.yml && chown root:root /etc/cloneDebRepo/cloneDebRepo.yml
COPY cloneDebRepo /bin/
RUN chmod 0755 /bin/cloneDebRepo && chown root:root /bin/cloneDebRepo

COPY makeRepos /bin/
RUN chmod 0755 /bin/makeRepos && chown root:root /bin/makeRepos

RUN mkdir -p /reposync
VOLUME /reposync
WORKDIR /reposync

ENTRYPOINT [ "/bin/makeRepos" ]
