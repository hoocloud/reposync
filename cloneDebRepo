#!/usr/bin/env python3
import sys
import os
import logging
import pprint
import argparse
import subprocess

import pathlib

import yaml
import deepmerge


scriptName = sys.argv[0]
scriptBasename = os.path.basename(scriptName)
scriptDirname = os.path.dirname(os.path.abspath(scriptName))

logging.basicConfig(level=logging.NOTSET,
                    format = '%(asctime)s.%(msecs)03d %(levelname)s %(module)s '
                             '%(funcName)s: %(message)s',
                    datefmt = '%Y-%m-%d %H:%M:%S')
log=logging.getLogger()

class Clone(object):
    def __init__(self,
            configFiles):
        self.configFiles = configFiles

    @staticmethod
    def firstFound(fs):
        log.debug('fs: {}'.format(pprint.pformat(fs)))
        for f in fs:
            if pathlib.Path(f).is_file():
                return f
        return None

    def getRepoConfig(self, repo, **kwargs):
        defaultConfig = self.getCloneConfig(**kwargs).get('default', {})
        repoConfig = self.getCloneConfig(**kwargs).get('repos', {}).get(repo, {})
        self._mergeConfig(defaultConfig, repoConfig, **kwargs)
        return defaultConfig

    def getCloneConfig(self, **kwargs):
        return self._getConfigFromFile(self.firstFound(self.configFiles))

    def _getConfigFromFile(self, configFile):
        log.debug('configFile: {}'.format(configFile))
        try:
            with open(configFile, "r") as f:
                return yaml.safe_load(f)
        except IOError as e:
            errno, strerror = e.args
            sys.exit('configFile {}: I/O error({}): \n{}\n\n'.format( configFile, errno, pprint.pformat(strerror) ) )
        except yaml.YAMLError as e:
            sys.exit('configFile {}: Invalid YAML: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) ) )

        except Exception as e:
            sys.exit('configFile {}: could not be read with error: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) ) )

    def _mergeConfig(self, *configs, **kwargs):
        # possible values: append override prepend
        listStrategy = kwargs.get('listStrategy', 'append')
        # possible values: merge override
        dictStrategy = kwargs.get('dictStrategy', 'merge')
        # possible values: override use_existing
        fallbackStrategy = kwargs.get('fallbackStrategy', 'override')
        # possible values: override override_if_not_empty use_existing
        conflictStrategy = kwargs.get('conflictStrategy', 'override')
         
        log.debug('configs\n{}'.format( pprint.pformat(configs) ))

        configMerger = deepmerge.Merger(
            [
                (list, [listStrategy]),
                (dict, [dictStrategy])
            ],
            [fallbackStrategy],
            [conflictStrategy]
        )
        config0 = configs[0]
        log.debug('config0:\n{}\n\n'.format( pprint.pformat(config0) ))
        for config in configs[1:]:
            configMerger.merge( config0, config )
            log.debug('config:\n{}\n\n'.format( pprint.pformat(config) ))
            log.debug('merged config0: \n{}\n\n'.format( pprint.pformat(config0) ))



def main():
    global scriptName
    global scriptBasename
    global scriptDirname
    log.debug('scriptName: \"{}\"'.format(scriptName))
    log.debug('scriptBasename: \"{}\"'.format(scriptBasename))
    log.debug('scriptDirname: \"{}\"'.format(scriptDirname))

    parser = argparse.ArgumentParser(prog = scriptBasename)
    parser.add_argument('--configFile',
                        default = '/etc/' + scriptBasename + '/' + scriptBasename + '.yml',
                        dest =  'configFile',
                        help = ('the configFile'),
                       )
    parser.add_argument('--repoDir',
                        default = '/reposync/debian',
                        dest =  'repoDir',
                        help = ('the repoDir'),
                       )
    args = parser.parse_args()

    configFile = args.configFile
    repoDir = args.repoDir

    configFiles = ( scriptDirname + '/' + scriptBasename + '.yml', configFile )

    clone = Clone(configFiles = configFiles)

    log.debug('clone config:\n{}\n'.format(pprint.pformat(clone.getCloneConfig())))
    log.debug('repo dir: {}'.format(repoDir))

    os.makedirs(name = repoDir, mode = 0o755, exist_ok=True)

    for repo in clone.getCloneConfig().get('repos', {}).keys():
        log.debug('repo: {}'.format(repo))
        
        repoConfig = clone.getRepoConfig(repo = repo)
        log.debug('repoConfig:\n{}\n'.format(pprint.pformat(repoConfig)))
   
        pkgMirrorProtocol = repoConfig.get('pkgMirrorProtocol')
        pkgMirrorHost     = repoConfig.get('pkgMirrorHost')
        pkgMirrorRepoDir  = repoConfig.get('pkgMirrorRepoDir')
                                         

        codenames         = repoConfig.get('codenames')
        architectures         = repoConfig.get('architectures')
        components            = repoConfig.get('components')
        extraOpts            = repoConfig.get('extraOpts')

        # linux + initrd
        print('commands for repo {}:'.format(repo))

        # create repo
        debMirrorCmd = [
                       '/usr/bin/debmirror',
                       '--diff=none',
                       '-p',
                       '-v',
                       '--no-check-gpg',
                       '--ignore-release-gpg',
                       '--nosource',
                       '--rsync-extra=none',
                       '-e', pkgMirrorProtocol,
                       '-h', pkgMirrorHost, 
                       '-r', pkgMirrorRepoDir,
                       '-a', ','.join(architectures),
                       '-d', ','.join(codenames),
                       '-s', ','.join(components),
                       ] 
        debMirrorCmd = debMirrorCmd + extraOpts
        debMirrorCmd = debMirrorCmd + [ repoDir + '/' + repo + '/', ]
        log.debug('debMirrorCmd:\n    {}\n'.format(pprint.pformat(debMirrorCmd)))


        subprocess.run(debMirrorCmd)
if __name__ == '__main__':
    sys.exit(main())
    
