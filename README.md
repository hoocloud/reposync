## Container for syncing OS repositories (f.e. deb)

use this container as follows
```
./runReposync
```





push README as description to quay.io f.e.
```
podman run --rm -t -v $(pwd):/myvol -e APIKEY__QUAY_IO="$(APIKEY__QUAY_IO)" chko/docker-pushrm:1 --file /myvol/README.md --provider quay --debug $(REG)/$(REG_NS)/reposync
```
